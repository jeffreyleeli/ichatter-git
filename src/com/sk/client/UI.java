package com.sk.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class UI extends JFrame {

	public static void main(String[] args) {
		(new UI()).launch();
	}

	private void launch() {
		organizeComponents();
		setLocationAndSize();
		addActionListeners();

		setVisible(true);
	}

	private void organizeComponents() {
		textArea = new JTextArea();
		textArea.setColumns(50);
		textArea.setRows(20);
		textArea.setEditable(false);

		textField = new JTextField();
		textField.setFocusable(true);

		add(textArea, BorderLayout.NORTH);
		add(textField, BorderLayout.SOUTH);

		pack();
	}

	private void setLocationAndSize() {
		Toolkit toolkit = getToolkit();
		Dimension screenSize = toolkit.getScreenSize();

		setLocation((screenSize.width - getWidth()) / 2,
				(screenSize.height - getHeight()) / 2);
	}

	private void addActionListeners() {
		textField.addActionListener(new TextFieldListener());

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private class TextFieldListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			Date now = new Date();
			textArea.append(textField.getText() + "\t(" + now + ")" + "\n");

			textField.setText("");
		}
	}

	private JTextArea textArea;
	private JTextField textField;
}
